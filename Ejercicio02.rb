require 'date'

class Course
  attr_reader :name, :start, :finish
  # Constructor
  def initialize(name, start, finish)
    @name = name
    @start = Date.parse(start)
    @finish = Date.parse(finish)
  end

  # Método de clase para leer el archivo
  def self.read_file(filename)
    data = File.open(filename, 'r') { |file| file.readlines.map(&:chomp) }
    
    courses = []
    data.each do |e|
      ls = e.split(', ')
      courses << Course.new(*ls)
    end

    return courses
  end

  # Revisar si la fecha es menor o igual a '01-01-2018'
  def check_date_2018(date)
    date >= Date.parse('2018-01-01') # Devuélve 'true' o 'false'
  end

  # Revisar si la fecha de inicio de curso es previo a la fecha entregada
  def before_to(date)
    # Levanta una excepción en caso que el argumento fecha sea mayor a 2018-01-01.
    raise ArgumentError.new('Argument must not be year 2018 or above') if check_date_2018(date)
    # Retorna 'true' si fecha de inicio (@start) es menor a la fecha entregada como argumento
    @start < date
  end

  # Revisar si la fecha de fin de curso, es posterior a la fecha entregada
  def after_to(date)
    # Levanta una excepción en caso que el argumento fecha sea mayor a 2018-01-01.
    raise ArgumentError.new('Argument must not be year 2018 or above') if check_date_2018(date)
    # Retorna 'true' si la fecha de termino (@finish) es mayor a la fecha entregada como argumento
    @finish > date
  end
end

# Cursos que inician antes de la fecha ingresada:
def courses_before_to(courses, date)
  array = []
  courses.each { |e| array.push(e.name) if e.before_to(date) }
  array.each { |e| puts "- #{e}"}
end

# Cursos que finalizan después de la fecha ingresada:
def courses_after_to(courses, date)
  array = []
  courses.each { |e| array.push(e.name) if e.after_to(date) }
  array.each { |e| puts "- #{e}"}
end

# Fecha:
date = Date.parse('2017-04-22')

# Llamado a método de clase para leer los cursos.
courses = Course.read_file('cursos.txt')
courses.each { |e| puts "#{e.name} => #{e.start} - #{e.finish}" }

# Saber qué cursos comienzan previo a una fecha entregada como argumento.
puts "Los cursos que comienzan antes de #{date} son:"
courses_before_to(courses, date)

# Saber qué cursos finalizan posterior a una fecha entregada como argumento.
puts "Los cursos que finalizan después de #{date} son:"
courses_after_to(courses, date)