class Table
  attr_reader :name, :earnings
  # Constructor:
  def initialize(name, *earnings)
    @name = name
    @earnings = earnings.map(&:to_i)
  end

  # método de clase para leer el archivo 'casino.txt'
  def self.read_file(filename)
    table = File.open(filename, 'r') { |file| file.readlines.map(&:chomp) }
    array = []

    table.each do |e|
      ls = e.split(', ')
      array << Table.new(*ls)
    end

    return array
  end

  # Conocer el mejor día de ganacia de cada mesa
  def best_day
    value_max = @earnings.max # valor más alto del arreglo
    best_day = @earnings.index(value_max) # indice que tiene el valor más alto (capturar el día)
    return { day: best_day, value: value_max }
  end

  # Conocer el mayor valor recaudado, que mesa y día corresponde.
  def self.higher_value(tables)
    max = { day: nil, value: 0}
    name = ''

    tables.each do |e|
      if e.best_day[:value] > max[:value]
        max = e.best_day
        name = e.name
      end
    end

    return "Mesa: #{name}, día: #{max[:day] + 1}, total: #{max[:value]}"
  end

  # Promedio
  def average
    @earnings.inject(&:+) / @earnings.size.to_f
  end

  # Promedio de todas las mesas
  def self.averages(tables)
    array = []
    tables.each do |e|
      array << "#{e.name} => #{e.average}"
    end

    return array
  end
end

# llamada a método de clase que permite leer el archivo e instanciar una mesa por línea del archivo.
tables = Table.read_file('casino.txt')
tables.each { |e| puts "#{e.name} => #{e.earnings}" }

# llamada a método de clase para conocer el mayor valor recaudado, a que mesa y día corresponde.
best_value = Table.higher_value(tables)
puts best_value # => Mesa: Mesa 4, día: 2, total: 93

# llamada a método de clase para calcular el promedio total de lo recaudado por todas las mesas en todos los días.
averages = Table.averages(tables)
puts averages